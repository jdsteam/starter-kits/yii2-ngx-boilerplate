<?php

return [
    'guitar0' => [
        'brand' => 'Gibson',
        'model' => 'Gibson Les Paul SG'
    ],
    'guitar1' => [
        'brand' => 'Yamaha',
        'model' => 'Revstar'
    ],
    'guitar2' => [
        'brand' => 'Fender',
        'model' => 'Deluxe Strat'
    ]
];

export default {
  currentUser: {
    id: null,
    username: null,
    role: null,
    name: null,
    email: null
  }
}

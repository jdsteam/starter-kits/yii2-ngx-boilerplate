export const API_URL = process.env.VUE_APP_API_URL
export const ACCESS_TOKEN_PREFIX = process.env.VUE_APP_ACCESS_TOKEN_PREFIX
export const SITE_TITLE = 'Frontend App'

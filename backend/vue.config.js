const path = require('path')

module.exports = {
  publicPath: '/backend/',
  devServer: { 
  	watchOptions: { poll: true },
    disableHostCheck: true
  },
  pluginOptions: {
    'style-resources-loader': {
      preProcessor: 'scss',
      patterns: []
    }
  }
}

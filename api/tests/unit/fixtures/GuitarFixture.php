<?php
namespace app\tests\unit\fixtures;

use yii\test\ActiveFixture;

class GuitarFixture extends ActiveFixture
{
    public $modelClass = 'app\models\Guitar';
}
